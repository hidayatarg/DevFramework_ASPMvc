﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevFramework.Northwind.Entities.Concrete;
using FluentNHibernate.Mapping;

namespace DevFramework.Northwind.DataAccess.Concrete.NHibernate.Mappings
{
   public class CategoryMap: ClassMap<Category>
    {
        //with help of constructor we specifiy this object will go to which table
        public CategoryMap()
        {
            Table(@"Categories");

            //Enable lazy loading
            LazyLoad();

            Id(x => x.CategoryId).Column("CategoryID");

            Map(x => x.CategoryName).Column("CategoryName");
           

        }
    }
}
