﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using DevFramework.Core.DataAccess.EntityFramework;
using DevFramework.Northwind.DataAccess.Abstruct;
using DevFramework.Northwind.Entities.ComplexTypes;
using DevFramework.Northwind.Entities.Concrete;

namespace DevFramework.Northwind.DataAccess.Concrete.EntityFramework
{
    //Entityframework for the ProductDal
    //SOLID -D Dependency inversion 
    //NOTE:it is implemented with IProductDal and IProductDal is implement with IEntityRepository<Product> 
    //All operations all implemented in the IEntityRepository.

    public class EfProductDal :IEfEntityRepositoryBase<Product,NorthwindContext>, IProductDal
    {
        //Joining Product and Category 
        

        public List<ProductDetails> GetProductDetails()
        {
            using (NorthwindContext context= new NorthwindContext())
            {
                var result = from p in context.Products
                    join c in context.Categories on p.CategoryId equals c.CategoryId
                    select new ProductDetails
                    {
                        ProductId = p.ProductId,
                        ProductName = p.ProductName,
                        CategoryName = c.CategoryName
                    };

                return result.ToList();
            }
        }
    }
}
