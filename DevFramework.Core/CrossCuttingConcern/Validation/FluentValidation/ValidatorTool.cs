﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;

namespace DevFramework.Core.CrossCuttingConcern.Validation.FluentValidation
{
    public class ValidatorTool
    {
        public static void FlentValidate(IValidator validator, object entity  )
        {
            var result = validator.Validate(entity);

            if (result.Errors.Count>0)
            {
                //if there is an exception 
                //return each of the exception togather in a validation exception 
                throw new ValidationException(result.Errors);
            }
        }
    }
}
