﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevFramework.Core.Entities;
using System.Data.Entity;

namespace DevFramework.Core.DataAccess.EntityFramework
{
    
    //Before closing the context are used for queryings
    public class EfQueryableRepository<T> : IQueryableRepository<T> where T : class, IEntity, new()
    {
        private DbContext _context;
        private IDbSet<T> _entities;
      
        public EfQueryableRepository(DbContext context)
        {
            _context = context;
        }

        //For any input object.
        public IQueryable<T> Table => this.Entities;

        protected virtual IDbSet<T> Entities
        {

            get
            {
                if (_entities == null)
                    _entities = _context.Set<T>();
                return _entities;
            }
        }
    }
}
