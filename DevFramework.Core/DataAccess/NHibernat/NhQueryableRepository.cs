﻿using DevFramework.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Linq;

namespace DevFramework.Core.DataAccess.NHibernat
{
    public class NhQueryableRepository<T> : IQueryableRepository<T>
         where T : class, IEntity, new()
    {
        private NHibernateHelper _nHibernateHelper;
        private IQueryable<T> _entities;

        //usiing dependency injection
        public NhQueryableRepository(NHibernateHelper nHibernateHelper)
        {

            _nHibernateHelper = nHibernateHelper;

        }

        public IQueryable<T> Table
        {
            get
            {
                return this._entities;
            }
        }

        public virtual IQueryable<T> Entites
        {
            get
            {
                if (_entities == null)
                {
                    //Subscribing to All 
                    _entities = _nHibernateHelper.OpenSession().Query<T>();
                }
                //else will return the avaliable one
                return _entities;
            }
        }
    }
}
