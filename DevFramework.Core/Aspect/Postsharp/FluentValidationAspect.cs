﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using DevFramework.Core.CrossCuttingConcern.Validation.FluentValidation;
using FluentValidation;
using PostSharp.Aspects;

namespace DevFramework.Core.Aspect.Postsharp
{
    [Serializable]
  public  class FluentValidationAspect:OnMethodBoundaryAspect
    {  
        
        //call validator tool
        Type _validatorType;

        public FluentValidationAspect(Type validatorType)
        {
            _validatorType = validatorType;
        }


        //on the entery of method //Validate when enterying the method
        public override void OnEntry(MethodExecutionArgs args)
        {
            //validation information is in the validator type
            //because we dont know the type of validation data *casting inorder to use as IValidator
            var validator = (IValidator)Activator.CreateInstance(_validatorType);

            //BaseType abstruct
            var entityType = _validatorType.BaseType.GetGenericArguments()[0];

            //check all argument if the type is product add it in the entites
            var entities = args.Arguments.Where(t => t.GetType() == entityType);


            foreach (var entity in entities)
            {
                ValidatorTool.FlentValidate(validator,entity);
            }
        }
    }
}
