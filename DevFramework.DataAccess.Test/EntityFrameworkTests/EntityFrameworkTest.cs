﻿using System;
using DevFramework.Northwind.DataAccess.Concrete.EntityFramework;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DevFramework.DataAccess.Test.EntityFrameworkTests
{
    //To test if the EfProductDal list all element and list according to the filter
    [TestClass]
    public class EntityFrameworkTest
    {
        [TestMethod]
        public void Get_all_returns_all_Products()
        {
            //Normally taking instance of class among the layer is prohibited but since it is test for now
            EfProductDal productDal= new EfProductDal();
            var result = productDal.GetList();

            Assert.AreEqual(77,result.Count);

        }

        //for filter products
        [TestMethod]
        public void Get_all_with_parameters_returns_filtered_Products()
        {
            //Normally taking instance of class among the layer is prohibited but since it is test for now
            EfProductDal productDal = new EfProductDal();
            var result = productDal.GetList(p=>p.ProductName.Contains("ab"));

            Assert.AreEqual(4, result.Count);

        }
    }
}
