﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevFramework.Core.CrossCuttingConcern.Validation.FluentValidation;
using DevFramework.Northwind.Business.Abstruct;
using DevFramework.Northwind.Business.ValidationRules.FluentValidation;
using DevFramework.Northwind.DataAccess.Abstruct;
using DevFramework.Northwind.Entities.Concrete;
using DevFramework.Core.Aspect.Postsharp;
using PostSharp;

namespace DevFramework.Northwind.Business.Concrete.Managers
{
    public class ProductManager : IProductService
    {
        private IProductDal _productDal;
        private IProductDal @object;

        public ProductManager(IProductDal @object)
        {
            this.@object = @object;
        }

        public List<Product> GetAll()
        {
            return _productDal.GetList();
        }

        public Product GetById(int id)
        {
            return _productDal.Get(p => p.ProductId == id);

        }

        [FluentValidationAspect(typeof(ProductValidator))]
        public Product Add(Product product)
        {
            //Validation before
            return _productDal.Add(product);
        }


        [FluentValidationAspect(typeof(ProductValidator))]
        public Product Update(Product product)
        {
           return _productDal.Update(product);
        }
    }
}
