﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevFramework.Northwind.Entities.Concrete;
using FluentValidation;

namespace DevFramework.Northwind.Business.ValidationRules.FluentValidation
{
   public class ProductValidator:AbstractValidator<Product>
    {
        public ProductValidator()
        {
            //Customize according to your work
            RuleFor(p => p.CategoryId).NotEmpty().WithMessage("Canot Left empty"); //standard message
            RuleFor(p => p.ProductName).NotEmpty();
            RuleFor(p => p.UnitPrice).GreaterThan(0);
            RuleFor(p => p.QuantityPerUnit).NotEmpty();
            RuleFor(p => p.ProductName).Length(2, 20); //min 2 max 20
            RuleFor(p => p.UnitPrice).GreaterThan(20).When(p => p.CategoryId == 1); //when category id is 1 it should be greater than 20.
          //  RuleFor(p => p.ProductName).Must(StartwithA);

        }

        private bool StartwithA(string arg)
        {
            return arg.StartsWith("A");
        }
    }
}
